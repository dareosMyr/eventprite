class AddTokenDigestToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :token_digest, :string
  end
end
