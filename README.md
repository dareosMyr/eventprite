# README

This project is part 2 of the Ruby on Rails course for Associations provided by The Odin Project.

Link to Associations project page: https://www.theodinproject.com/courses/ruby-on-rails/lessons/associations

The purpose of this project is to make a sample version of the website Eventbrite in order to learn more about Rails ActiveRecord associations. As with most of these projects, design is secondary to functionality.

Users can create accounts with username and email which in turn can create events with a location and date. They can also attend events that themselves or others create. This is accomplished using a many to many relationship through a join table between the Users group and Event group.

ActiveRecord scopes are used to seperate events into past and future ones.

Sign in is accomplished simply by checking that the provided username exists and then creates a randomly generated, encrypted session token and stores it in the database and in the sessions hash.