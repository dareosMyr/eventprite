class Event < ApplicationRecord
  belongs_to :creator, class_name: "User"
  has_many :joins, foreign_key: 'event_id'
  has_many :attendees, through: :joins, source: :user
  scope :upcoming_events, -> { where('date > ?', Time.now) }
  scope :past_events, -> { where('date < ?', Time.now) }

  def date_in_words
    self.date.strftime('%A, %b %d, %Y')
  end
end
