class User < ApplicationRecord
  before_create :build_token
  has_many :events, foreign_key: 'creator_id', class_name: 'Event'
  has_many :joins, foreign_key: 'user_id'
  has_many :attended_events, through: :joins, source: :event

  def build_token
    if self.token_digest.nil?
      self.token_digest = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
    else
      self.update_attribute(:token_digest, Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64))
    end
  end
  
end
