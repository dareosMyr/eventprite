class UsersController < ApplicationController
  def show
    @user = User.find_by(token_digest: session[:token])
    @upcoming_events = @user.attended_events.upcoming_events
    @past_events = @user.attended_events.past_events
  end

  def new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in(@user)
      redirect_to @user
    else
      flash.now[:error] = 'Invalid info'
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email)
  end
end
