class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(name: params[:session][:name])
    if @user
      @user.build_token
      log_in(@user)
      redirect_to @user
    else
      flash.now[:error] = 'User doesn\'t exist'
      render 'new'
    end
  end

  def destroy
    session.delete(:token)
    redirect_to root_path
  end
end
