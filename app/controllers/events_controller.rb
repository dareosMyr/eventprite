class EventsController < ApplicationController
  def new
  end

  def show
    @user = get_user
    @event = Event.find_by(id: params[:id])
  end

  def index
    @events = Event.all
  end

  def create
    @user = User.find_by(token_digest: session[:token])
    @event = @user.events.build(event_params)
    if @event.save
      flash[:success] = 'Event created!'
      redirect_to @event
    else
      flash[:error] = 'Invalid fields'
      render 'new'
    end
  end

  def add_attendee
    @event = Event.find_by(id: params[:id])
    @user = User.find_by(token_digest: session[:token])
    if @event.attendees.reload.where(name: @user.name).any?
      flash[:error] = 'You are already on the list!'
      redirect_to @event
    else
      @event.attendees << @user
      redirect_to @event
    end
  end

  def remove_attendee
    @event = Event.find_by(id: params[:id])
    @user = User.find_by(token_digest: session[:token])
    @event.attendees.delete(@user)
    redirect_to @event
  end


  private

    def event_params
      params.require(:event).permit(:location, :date)
    end
end
