module SessionsHelper
  def log_in(user)
    session[:token] = user.token_digest
  end

  def get_user
    @user ||= User.find_by(token_digest: session[:token])
  end

  def logged_in?
    !!get_user
  end
end
