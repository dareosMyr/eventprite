Rails.application.routes.draw do
  root 'static_pages#home'
  get '/login', to: 'sessions#new'
  get '/logout', to: 'sessions#destroy'
  post '/login', to: 'sessions#create'
  resources :users, only: [:create, :new, :show]
  resources :events, only: [:new, :show, :index, :create]
  get '/events/:id/add_attendee', to: 'events#add_attendee', as: 'add_attendee'
  get '/events/:id/remove_attendee', to: 'events#remove_attendee', as: 'remove_attendee'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
